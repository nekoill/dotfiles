# XMonad & XMobar builds and other dotfiles

## NOTES
+ On Arch, XMonad & XMobar should be built via stack, because Arch has issues with dynamic Haskell packages, it's a fairly easy process when you have instructions which can be found here:
[Build Haskell packages with stack](https://brianbuccola.com/how-to-install-xmonad-and-xmobar-via-stack/)
+ Right now there's only the most crucial files, and mostly for XMonad & XMobar; other system dotfiles and personal configs might be added later, but those are even more of a hot garbage I cobbled together from smarter people configs

## LINKS
Highly advised to visit these guys repos, they know what they're doing way better than I am:

+ [Derek Taylor aka DistroTube aka DT, awesome YouTube channel, lots of XMonad related videos & tutorials, my build of XMonad is mostly his build but worse](https://gitlab.com/dwt1/dotfiles)
+ [That guy that bullied me into using Arch](https://github.com/LukeSmithxyz)
+ [Looks like a cool build of XMonad, lots of awesome features, interactive XMobar w/ pop-up menus & stuff, but way outta my depth; still stole a couple of scripts from him anyway](https://github.com/Haron-Prime/My_config_files)
