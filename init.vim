set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath
let g:mapleader = "\<Space>"

syntax enable
set encoding=utf-8                                 "required to keep multiple buffers open
set fileencoding=utf-8                             "writes encoding to file
set pumheight=10                                   "makes popup menu smaller
set ruler                                          "show the cursor position at all times
set cmdheight=2                                    "more space for message display
set t_Co=256                                       "support for 256 color
set tabstop=2                                      "insert 2 spaces per tab
set sts=2
set smarttab
set expandtab
set smartindent
set sm
set autoindent
set laststatus=1                                   "always display the status line
set showtabline=2                                  "always show tabs
set updatetime=300                                 "faster completion(?)
set formatoptions-=cro                             "stop newline comment continuation
set wrap
set acd
set is
set ic
set scs
set nu
set rnu
set hls
set cul
set spr
set title
set mouse=a
set slm=mouse
set sta
set bin
set icon
set clipboard=unnamedplus
set sft
set sw=4
set guifont=:h17

call plug#begin('~/.config/nvim/plugged')
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
" HASKELL PLUGINS
Plug 'lukerandall/haskellmode-vim'
Plug 'eagletmt/neco-ghc'

" HASKELL PLUGINS

Plug 'yuki-ycino/fzf-preview.vim', { 'branch': 'release/remote', 'do': ':UpdateRemotePlugins' }
Plug 'airblade/vim-gitgutter'
Plug 'ctrlpvim/ctrlp.vim' " fuzzy find files
Plug 'scrooloose/nerdcommenter'
Plug 'neovimhaskell/haskell-vim'
Plug 'xabikos/vscode-javascript'
Plug 'neoclide/coc.nvim'
Plug 'junegunn/goyo.vim'
Plug 'cespare/vim-toml'
Plug 'wincent/terminus'
Plug 'neoclide/coc-eslint'
Plug 'norcalli/nvim-colorizer.lua'
Plug 'mg979/vim-visual-multi', {'branch': 'master'}
Plug 'mattn/emmet-vim'
Plug 'vmchale/dhall-vim'
Plug 'tpope/vim-surround'
Plug 'prettier/vim-prettier'
Plug 'grvcoelho/vim-javascript-snippets'
Plug 'w0rp/ale'
Plug 'isruslan/vim-es6'
Plug 'ryuta69/elly.vim'
Plug 'dracula/vim'
Plug 'franbach/miramare'
Plug 'liuchengxu/space-vim-dark'
Plug 'glepnir/oceanic-material'
Plug 'rafi/awesome-vim-colorschemes'
Plug 'nanotech/jellybeans.vim'
Plug 'ayu-theme/ayu-vim'
Plug 'christianchiarulli/nvcode-color-schemes.vim'
Plug 'kaicataldo/material.vim', { 'branch': 'main' }
Plug 'tomasiser/vim-code-dark'
Plug 'flazz/vim-colorschemes'
Plug 'chun-yang/auto-pairs'
Plug 'dracula/dracula-theme'
Plug 'eagletmt/neco-ghc'
Plug 'junegunn/vim-easy-align'
Plug 'https://github.com/junegunn/vim-github-dashboard'
Plug 'shougo/neocomplete.vim'
Plug 'neoclide/coc.nvim', {'do': 'yarn install --frozen-lockfile'}
Plug 'honza/vim-snippets'
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'tpope/vim-markdown'
Plug 'ap/vim-css-color'
Plug 'ryanoasis/vim-devicons'
call plug#end()

inoremap jk <ESC>
nmap <C-v> :NERDTreeToggle<CR>
vmap ++ <plug>NERDCommenterToggle
nmap ++ <plug>NERDCommenterToggle

let g:haddock_browser="/usr/bin/brave"
let g:ghc="/usr/bin/ghc-8.10.5"

let g:haskell_enable_quantification = 1   " to enable highlighting of `forall`
let g:haskell_enable_recursivedo = 1      " to enable highlighting of `mdo` and `rec`
let g:haskell_enable_arrowsyntax = 1      " to enable highlighting of `proc`
let g:haskell_enable_pattern_synonyms = 1 " to enable highlighting of `pattern`
let g:haskell_enable_typeroles = 1        " to enable highlighting of type roles
let g:haskell_enable_static_pointers = 1  " to enable highlighting of `static`
let g:haskell_backpack = 1                " to enable highlighting of backpack keywords

"HASKELL INDENTATION (for different cases):
let g:haskell_indent_if = 3
let g:haskell_indent_case = 2
let g:haskell_indent_let = 4
let g:haskell_indent_where = 6
let g:haskell_indent_before_where = 2
let g:haskell_indent_do = 3
let g:haskell_indent_in = 1
let g:haskell_indent_guard = 2
let g:haskell_indent_case_alternative = 1

"!!!COLORSCHEMES!!!"
"colorscheme ayu
"colorscheme aurora
"colorscheme dracula_bold
"colorscheme gotham256
"colorscheme bubblegum-256-dark
"colorscheme tigrana-256-dark
 colorscheme archery
"colorscheme vimbrains
"colorscheme mango
"colorscheme deus
"colorscheme gruvbox


" For Neovim 0.1.3 and 0.1.4 - https://github.com/neovim/neovim/pull/2198
if (has('nvim'))
  let $NVIM_TUI_ENABLE_TRUE_COLOR = 1
endif
" For Neovim > 0.1.5 and Vim > patch 7.4.1799 - https://github.com/vim/vim/commit/61be73bb0f965a895bfb064ea3e55476ac175162
" Based on Vim patch 7.4.1770 (`guicolors` option) - https://github.com/vim/vim/commit/8a633e3427b47286869aa4b96f2bfc1fe65b25cd
" https://github.com/neovim/neovim/wiki/Following-HEAD#20160511
if (has('termguicolors'))
  set termguicolors
endif

lua require'colorizer'.setup()


let g:NERDTreeGitStatusWithFlags = 1
let g:NERDTreeIgnore = ['^node_modules$']


" vim-prettier
"let g:prettier#quickfix_enabled = 1
"let g:prettier#quickfix_auto_focus = 0
" prettier command for coc
"command! -nargs=0 Prettier :CocCommand prettier.formatFile
" run prettier on save
"let g:prettier#autoformat = 0
"autocmd BufWritePre *.js,*.jsx,*.mjs,*.ts,*.tsx,*.css,*.less,*.scss,*.json,*.graphql,*.md,*.vue,*.yaml,*.html PrettierAsync


" ctrlp
let g:ctrlp_user_command = ['.git/', 'git --git-dir=%s/.git ls-files -oc --exclude-standard']

" j/k will move virtual lines (lines that wrap)
"noremap <silent> <expr> j (v:count == 0 ? 'gj' : 'j')
"noremap <silent> <expr> k (v:count == 0 ? 'gk' : 'k')

" sync open file with NERDTree
" " Check if NERDTree is open or active
"function! IsNERDTreeOpen()        
"  return exists("t:NERDTreeBufName") && (bufwinnr(t:NERDTreeBufName) != -1)
"endfunction

" Call NERDTreeFind if NERDTree is active, current window contains a modifiable
" file, and we're not in vimdiff
"function! SyncTree()
"  if &modifiable && IsNERDTreeOpen() && strlen(expand('%')) > 0 && !&diff
"    NERDTreeFind
"    wincmd p
"  endif
"endfunction

" Highlight currently open buffer in NERDTree
"autocmd BufEnter * call SyncTree()

" coc config
let g:coc_global_extensions = [
	\ 'coc-snippets',
  \ 'coc-fzf-preview',
  \ 'coc-highlight',
  \ 'coc-html',
  \ 'coc-markdownlint',
	\ 'coc-pairs',
  \ 'coc-sh',
	\ 'coc-eslint',
	\ 'coc-prettier',
	\ 'coc-json',
	\ ]
" from readme
" if hidden is not set, TextEdit might fail.

" Some servers have issues with backup files, see #649 set nobackup set nowritebackup / Better display for messages set cmdheight=2 " You will have bad experience for diagnostic messages when it's default 4000.
"set hidden
set updatetime=300

" don't give |ins-completion-menu| messages.
"set shortmess+=c

" always show signcolumns
set signcolumn=yes

" Use tab for trigger completion with characters ahead and navigate.
" Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-m>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-x>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current position.
" Coc only does snippet and additional edit on confirm.
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
" Or use `complete_info` if your vim support it, like:
" inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"

" Use `[g` and `]g` to navigate diagnostics
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Highlight symbol under cursor on CursorHold
autocmd CursorHold * silent call CocActionAsync('highlight')

" Remap for rename current word
nmap <F2> <Plug>(coc-rename)

" Remap for format selected region
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Remap for do codeAction of selected region, ex: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap for do codeAction of current line
nmap <leader>ac  <Plug>(coc-codeaction)
" Fix autofix problem of current line
nmap <leader>qf  <Plug>(coc-fix-current)

" Create mappings for function text object, requires document symbols feature of languageserver.
xmap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap if <Plug>(coc-funcobj-i)
omap af <Plug>(coc-funcobj-a)

" Use <C-f> for select selections ranges, needs server support, like: coc-tsserver, coc-python
xmap <silent> <C-f> <Plug>(coc-range-select)

" Use `:Format` to format current buffer
command! -nargs=0 Format :call CocAction('format')

" Use `:Fold` to fold current buffer
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" use `:OR` for organize import of current buffer
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" Add status line support, for integration with other plugin, checkout `:h coc-status`
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

" Using CocList
" Show all diagnostics
nnoremap <silent> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions
nnoremap <silent> <space>e  :<C-u>CocList extensions<cr>
" Show commands
nnoremap <silent> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document
nnoremap <silent> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols
nnoremap <silent> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list
nnoremap <silent> <space>p  :<C-u>CocListResume<CR>

let g:goyo_width=120
"let g:material_theme_style = 'darker-community'
"| 'palenight' | 'lighter' | 'ocean' | 'default-community' | 'palenight-community' | 'ocean-community' | 'lighter-community' | 'darker-community'
let g:material_terminal_italics = 1
let g:webdevicons_enable = 1
let g:webdevicons_enable_nerdtree = 1
let g:webdevicons_enable_airline_tabline = 1
let g:webdevicons_enable_airline_statusline = 1
let g:webdevicons_enable_ctrlp = 1
let g:neocomplete#enable_at_startup = 1
let g:airline#extensions#tabline#enabled=1
let g:airline_theme='abstract'
let g:deepspace_italics=1
let g:user_emmet_mode='inv'
let g:user_emmet_leader_key='<C-Z>'
"let g:oceanic_material_transparent_background=0
let g:oceanic_material_allow_bold=1
let g:oceanic_material_allow_italic=1
"let g:typescript_compiler_binary='tsc'
"let g:typescript_compiler_options='--lib es6'

" highlight Cursor guifg=steelblue guibg=orange
" highlight iCursor guifg=steelblue guibg=red
highlight iCursor gui=NONE guifg=bg guibg=red
set guicursor=n-v-c:block,i-ci-ve:ver10,r-cr:hor20,o:hor50
\,a:blinkwait700-blinkoff400-blinkon250-Cursor/lCursor
\,sm:block-blinkwait175-blinkoff150-blinkon175


"Notes for multi-cursor plugin mappings and such:
"!!!IMPORTANT!!!  
"If you spawn multiple cursors in a Normal Mode and enter Insert Mode,
"the cursors will disappear, but will still be in place

"Basic usage:
"select words with Ctrl-N (like Ctrl-d in Sublime Text/VS Code)
"create cursors vertically with Ctrl-Down/Ctrl-Up
"select one character at a time with Shift-Arrows
"press n/N to get next/previous occurrence
"press [/] to select next/previous cursor
"press q to skip current and get next occurrence
"press Q to remove current cursor/selection
"start insert mode with i,a,I,A
"Two main modes:
"in cursor mode commands work as they would in normal mode
"in extend mode commands work as they would in visual mode
"press Tab to switch between «cursor» and «extend» mode

" Make <CR> auto-select the first completion item and notify coc.nvim to
" format on enter, <cr> could be remapped by other vim plugin
inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

" Mappings for CoCList

" autocmd BufWritePost ~/.local/src/dwmblocks/config.h !cd ~/.local/src/dwmblocks/; sudo make install && { killall -q dwmblocks;setsid dwmblocks & }

"NEOVIDE TIEM
let g:neovide_refresh_rate=140
let g:neovide_transparency=0.8
let g:neovide_no_idle=v:true
let g:neovide_cursor_animation_length=0.13
let g:neovide_cursor_trail_length=0.6
let g:neovide_cursor_antialiasing=v:true
let g:neovide_cursor_vfx_mode="pixiedust"
let g:neovide_cursor_vfx_opacity=200.0
let g:neovide_cursor_vfx_particle_lifetime=1.2
let g:neovide_cursor_vfx_particle_density=10.0
let g:neovide_cursor_vfx_particle_speed=10.0
let g:neovide_cursor_vfx_particle_phase=1.5
let g:neovide_cursor_vfx_particle_curl=0.5
