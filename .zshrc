#!/usr/bin/env zsh

autoload -U colors && colors
#PROMPT="%{$fg[red]%}%n%{$reset_color%}@%{$fg[blue]%}%m %{$fg[yellow]%}%~ %{$reset_color%}%% "
#PS1=" %B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[magenta]%}@%{$fg[cyan]%}%M %{$fg[blue]%}%~%{$fg[red]%}]%{$reset_color%}$%b "

#export PATH=$PATH:$GOPATH/bin
export GOPATH=$HOME/go

# If you come from bash you might have to change your $PATH.
export PATH=$HOME/bin:/usr/local/bin:$PATH:$GOPATH/bin

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME=random

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in $ZSH/themes/
# If set to an empty array, this variable will have no effect.
ZSH_THEME_RANDOM_CANDIDATES=( 
							  "afowler"
							  "agnoster"
							  "avit"
							  "bureau"
							  "candy"
							  "crunch"
							  "cypher"
							  "dieter"
							  "gallois"
							  "gentoo"
							  "gianu"
							  "half-life"
							  "intheloop"
							  "jtriley"
							  "junkfood"
							  "kennethreitz"
							  "macovsky-ruby"
							  "mortalscumbag"
							  "muse"
							  "norm"
							  "obraun"
							  "pmcgee"
							  "re5et"
							  "refined"
	                          "robbyrussell"
							  "sonicradish"
							  "sunaku"
							  "takashiyoshida"
							  "wedisagree"
 							)

autoload -Uz compinit promptinit zfinit
compinit
promptinit
zfinit
autoload colors; colors
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)		# Include hidden files.

# Change cursor shape for different vi modes.
function zle-keymap-select () {
    case $KEYMAP in
        vicmd) echo -ne '\e[1 q';;      # block
        viins|main) echo -ne '\e[5 q';; # beam
    esac
}
zle -N zle-keymap-select
zle-line-init() {
    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne "\e[5 q"
}
zle -N zle-line-init
echo -ne '\e[5 q' # Use beam shape cursor on startup.
preexec() { echo -ne '\e[5 q' ;} # Use beam shape cursor for each new prompt.

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
HYPHEN_INSENSITIVE="true"

# Uncomment one of the following lines to change the auto-update behavior
# zstyle ':omz:update' mode disabled  # disable automatic updates
# zstyle ':omz:update' mode auto      # update automatically without asking
# zstyle ':omz:update' mode reminder  # just remind me to update when it's time

# Uncomment the following line to change how often to auto-update (in days).
# zstyle ':omz:update' frequency 13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# You can also set it to another string to have that shown instead of the default red dots.
# e.g. COMPLETION_WAITING_DOTS="%F{yellow}waiting...%f"
# Caution: this setting can cause issues with multiline prompts in zsh < 5.7.1 (see #5765)
COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
		  git
		  systemd
		  alias-finder
		  archlinux
		  battery
		  colored-man-pages
		  colorize
		  common-aliases
		  copybuffer
		  copydir
		  copyfile
		  docker
		  emoji
		  emoji-clock
		  fzf
		  git-prompt
		  lol
		  jsontools
		  themes
		  web-search
		  zsh-interactive-cd
		  zsh-navigation-tools
		)

source $ZSH/oh-my-zsh.sh

# User configuration

export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='nvim'
else
  export EDITOR='vim'
fi

# Compilation flags
export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
alias alm="alsamixer -c 0 -m"
alias auto="$HOME/.local/bin/auto.sh"
alias c="clear"
alias cgrp="doas ~/.local/bin/cgroupfs-mount"
alias downl="cd $HOME/Downloads/"
alias fap="sxiv -S 5 $HOME/wallpapers/fap"
alias geth="$HOME/Downloads/geth-linux-amd64-1.10.13-7a0c19f8/geth"
alias gotop="$HOME/.local/bin/gotop"
alias gotop="$HOME/Downloads/gotop/dist/gotop -c nekoill"
alias l="lsd -al"
alias lbin="cd $HOME/.local/bin/"
alias ls="lsd -al"
alias lsbl="lsblk -o NAME,FSSIZE,FSAVAIL,FSUSED,PATH,MOUNTPOINTS,PARTLABEL"
alias nuclear="$HOME/Downloads/nuclear-v0.6.17/nuclear"
alias pf="pacman -F --color always"
alias pfx="pacman -Fx --color always"
alias pfy="pacman -Fy"
alias pq="pacman -Q"
alias prc="doas pacman -Rcs"
alias psy="doas pacman -Syu"
alias rand="$HOME/random"
alias rcs="~/.local/bin/randcoloscript"
alias repo="cd $HOME/repo"
alias rng="ranger"
alias spc="doas pacman -Sc"
alias sps="doas pacman -S"
alias srng="doas ranger"
alias stx="startx"
alias sxk="setxkbmap -layout us,ru -option grp:caps_toggle &"
alias wtr="curl wttr.in/svo"
alias ydl="youtube-dl"
alias z="zsh"

#neofetch
# $HOME/random
#curl https://wttr.in/
